/**
 * 
 */
package Model.BO;

import java.util.ArrayList;

import Model.BEAN.HocSinh;
import Model.DAO.HocSinhDAO;

/**
 * @author Nguyen Ba Anh
 *
 */
public class HocSinhBO {

	HocSinhDAO hocSinhDAO;

	public HocSinhBO() {
		hocSinhDAO = new HocSinhDAO();
	}

	public ArrayList<HocSinh> layTatCaHS() {
		return hocSinhDAO.layTatCaHS();
	}

	public boolean xoaHocSinh(int thuTu) {
		return hocSinhDAO.xoaHocSinh(thuTu);
	}

	public boolean themHocSinh(HocSinh hocSinh) {
		// Kiểm tra số liệu nhập vào xem ổn chưa
		if (kiemTra(hocSinh)) {
			hocSinh.setXepLoai(tinhXepLoai(hocSinh.getDiemToan(),
					hocSinh.getDiemLy(), hocSinh.getDiemHoa()));
			return hocSinhDAO.themHocSinh(hocSinh);
		}

		else
			return false;
	}

	public boolean suaHocSinh(HocSinh hocSinh) {
		// Kiểm tra số liệu nhập vào xem ổn chưa
		if (kiemTra(hocSinh)) {
			hocSinh.setXepLoai(tinhXepLoai(hocSinh.getDiemToan(),
					hocSinh.getDiemLy(), hocSinh.getDiemHoa()));
			return hocSinhDAO.suaHocSinh(hocSinh);
		} else
			return false;
	}

	private String tinhXepLoai(float diemToan, float diemLy, float diemHoa) {
		float diemTrungBinh = (diemToan + diemLy + diemHoa) / 3;
		if (diemTrungBinh < 5 || diemToan < 5 || diemLy < 5 || diemHoa < 5)
			return "Yếu - " + String.valueOf(diemTrungBinh);
		else if (diemTrungBinh < 6.5 || diemToan < 6.5 || diemLy < 6.5
				|| diemHoa < 6.5)
			return "Trung Bình - " + String.valueOf(diemTrungBinh);
		else if (diemTrungBinh < 8 || diemToan < 8 || diemLy < 8 || diemHoa < 8)
			return "Khá - " + String.valueOf(diemTrungBinh);
		else if (diemTrungBinh < 9 || diemToan < 9 || diemLy < 9 || diemHoa < 9)
			return "Giỏi - " + String.valueOf(diemTrungBinh);
		else
			return "Xuất Sắc - " + String.valueOf(diemTrungBinh);
	}

	private boolean kiemTra(HocSinh hocSinh) {
		if (hocSinh.getTo() < 0)
			return false;
		if (hocSinh.getDiemToan() < 0 || hocSinh.getDiemHoa() < 0
				|| hocSinh.getDiemLy() < 0)
			return false;
		if (hocSinh.getDiemToan() > 10 || hocSinh.getDiemHoa() > 10
				|| hocSinh.getDiemHoa() > 10)
			return false;
		return true;
	}

}
