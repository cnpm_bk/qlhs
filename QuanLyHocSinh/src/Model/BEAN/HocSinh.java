/**
 * 
 */
package Model.BEAN;

/**
 * @author Nguyen Ba Anh
 *
 */
public class HocSinh {

	String hoTen, xepLoai;
	int thuTu, to;
	float diemToan, diemLy, diemHoa;
	
	public HocSinh() {
		
	}

	public HocSinh(String hoTen, String xepLoai, int thuTu, int to, float diemToan, float diemLy, float diemHoa) {
		super();
		this.hoTen = hoTen;
		this.xepLoai = xepLoai;
		this.thuTu = thuTu;
		this.to = to;
		this.diemToan = diemToan;
		this.diemLy = diemLy;
		this.diemHoa = diemHoa;
	}

	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public String getXepLoai() {
		return xepLoai;
	}

	public void setXepLoai(String xepLoai) {
		this.xepLoai = xepLoai;
	}

	public int getThuTu() {
		return thuTu;
	}

	public void setThuTu(int thuTu) {
		this.thuTu = thuTu;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public float getDiemToan() {
		return diemToan;
	}

	public void setDiemToan(float diemToan) {
		this.diemToan = diemToan;
	}

	public float getDiemLy() {
		return diemLy;
	}

	public void setDiemLy(float diemLy) {
		this.diemLy = diemLy;
	}

	public float getDiemHoa() {
		return diemHoa;
	}

	public void setDiemHoa(float diemHoa) {
		this.diemHoa = diemHoa;
	}
	
}
