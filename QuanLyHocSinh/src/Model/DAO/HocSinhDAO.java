/**
 * 
 */
package Model.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

import Model.BEAN.HocSinh;

/**
 * @author Nguyen Ba Anh
 *
 */
public class HocSinhDAO extends CoSoDuLieu {

	HocSinh hocSinh = new HocSinh();
	ArrayList<HocSinh> cacHocSinh = new ArrayList<HocSinh>();

	public HocSinhDAO() {
		super();
	}

	public ArrayList<HocSinh> layTatCaHS() {
		String query = "select * from HOCSINH";
		try {
			resultSet = truyVan(query);
			while (resultSet.next()) {
				int thuTu = Integer.parseInt(resultSet.getString("ThuTu"));
				String hoTen = resultSet.getString("HoTen");
				int to = Integer.parseInt(resultSet.getString("To"));
				float diemToan = Float.parseFloat(resultSet
						.getString("DiemToan"));
				float diemLy = Float.parseFloat(resultSet.getString("DiemLy"));
				float diemHoa = Float
						.parseFloat(resultSet.getString("DiemHoa"));
				String xepLoai = resultSet.getString("XepLoai");
				HocSinh hs = new HocSinh(hoTen, xepLoai, thuTu, to, diemToan,
						diemLy, diemHoa);
				cacHocSinh.add(hs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cacHocSinh;
	}

	public boolean xoaHocSinh(int thuTu) {
		//taoKetNoi();
		String query = "delete from HOCSINH where ThuTu=" + thuTu;
		try {
			capNhat(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean themHocSinh(HocSinh hs) {
		String query = "insert into HOCSINH values(" + hs.getThuTu() + ",'"
				+ hs.getHoTen() + "'," + hs.getTo() + "," + hs.getDiemToan()
				+ "," + hs.getDiemLy() + "," + hs.getDiemHoa() + ",'"
				+ hs.getXepLoai() + "')";
		try {
			capNhat(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean suaHocSinh(HocSinh hs) {
		String query = "update HOCSINH set HoTen='" + hs.getHoTen() + "', To=" + hs.getTo()
				+ ", DiemToan=" + hs.getDiemToan() + ", DiemLy=" + hs.getDiemLy() + ", DiemHoa="
				+ hs.getDiemHoa() + ", XepLoai='" + hs.getXepLoai() + "' where ThuTu=" + hs.getThuTu();
		try {
			capNhat(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


}
