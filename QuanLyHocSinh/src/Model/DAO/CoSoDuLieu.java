/**
 * 
 */
package Model.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Nguyen Ba Anh
 *
 */
public class CoSoDuLieu {
	Connection connection = null;
	Statement statement = null;
	ResultSet resultSet = null;

	// Constructor
	public CoSoDuLieu() {
		taoKetNoi();
	}

	public void taoKetNoi() {
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			String address = "jdbc:ucanaccess://E:\\Documents\\@ITF-DANANG\\Ki7\\CONG NGHE PHAN MEM\\@Web\\QuanLyHocSinh\\WebContent\\WEB-INF\\CoSoDuLieu.accdb";
			connection = DriverManager.getConnection(address);
			statement = connection.createStatement();
		} catch (Exception e) {
			System.err.println("[Database constructor] Lỗi: " + e);
		}
	}

	// Truy váº¥n cáº­p nháº­t
	public void capNhat(String query) throws SQLException {
		statement.executeUpdate(query);
	}

	// Truy váº¥n chá»�n
	public ResultSet truyVan(String query) throws SQLException {
		// ResultSet rs = null;
		resultSet = statement.executeQuery(query);
		return resultSet;
	}

	public void ngatKetNoi() throws SQLException {
		if (statement != null)
			statement.close();
		if (connection != null)
			connection.close();
		if (resultSet != null)
			resultSet.close();
	}

}
