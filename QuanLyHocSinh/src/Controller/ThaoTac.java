package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.BEAN.HocSinh;
import Model.BO.HocSinhBO;

/**
 * Servlet implementation class ThaoTac
 */
@WebServlet("/ThaoTac")
public class ThaoTac extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HocSinhBO hocSinhBO;
	HocSinh hocSinh;
	ArrayList<HocSinh> cacHocSinh;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ThaoTac() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		PrintWriter out = response.getWriter();

		hocSinhBO = new HocSinhBO();
		cacHocSinh = new ArrayList<HocSinh>();
		String guiBieuMau = request.getParameter("GuiBieuMau");
		if (guiBieuMau == null) {
			// Form không kích hoạt thì thực hiện chức năng XEM
			cacHocSinh = hocSinhBO.layTatCaHS();
			request.setAttribute("cacHocSinh", cacHocSinh);
			request.getRequestDispatcher("/WEB-INF/Xem.jsp").include(request,
					response);
		} else if ("Co".equals(guiBieuMau)) { // 3 chức năng còn lại
			String loaiBieuMau = request.getParameter("LoaiBieuMau");
			if (loaiBieuMau == null) {
				loaiBieuMau = ""; // Sua
			}
			switch (loaiBieuMau) {
			case "Them": { // THÊM
				int thuTu = 0;
				String hoTen = request.getParameter("HoTen");
				int to = 0;
				float diemToan = 0, diemLy = 0, diemHoa = 0;
				try {
					thuTu = Integer.parseInt(request.getParameter("ThuTu"));
					to = Integer.parseInt(request.getParameter("To"));
					diemToan = Float.parseFloat(request.getParameter("DiemToan"));
					diemLy = Float.parseFloat(request.getParameter("DiemLy"));
					diemHoa = Float.parseFloat(request.getParameter("DiemHoa"));
				} catch (NumberFormatException e) {
					e.printStackTrace();
					out.println("[Thêm] Xảy ra lỗi khi chuyển đổi số liệu: <i>" + e
							+ "</i><hr>");
					out.print("<a href=\"ThaoTac\">Quay lại</a>");
					return;
				}
				String xepLoai = ""; // xếp sau, dựa vào điểm nhập vào
				hocSinh = new HocSinh(hoTen, xepLoai, thuTu, to, diemToan, diemLy,
						diemHoa);
				if (hocSinhBO.themHocSinh(hocSinh)) {
					response.sendRedirect("ThaoTac");
				} else {
					out.print("Thêm thất bại!<hr> <a href=\"ThaoTac\"> Quay lai </a>");
				}
				break;
			}
			case "Xoa": { // XÓA
				int thuTu = Integer.parseInt(request.getParameter("ThuTu"));
				if (hocSinhBO.xoaHocSinh(thuTu)) {
					// response.sendRedirect("ThaoTac");
					out.print("Xóa thành công!<hr> <a href=\"ThaoTac\"> Quay lai </a>");
				} else {
					out.print("Không xóa được!<hr> <a href=\"ThaoTac\"> Quay lai </a>");
				}
				break;
			}
			default: {// Không phải 2 loại trên thì là chức năng SỬA
				int thuTu = 0;
				String hoTen = request.getParameter("HoTen");
				int to = 0;
				float diemToan = 0, diemLy = 0, diemHoa = 0;
				try {
					thuTu = Integer.parseInt(request.getParameter("ThuTu"));
					to = Integer.parseInt(request.getParameter("To"));
					diemToan = Float.parseFloat(request.getParameter("DiemToan"));
					diemLy = Float.parseFloat(request.getParameter("DiemLy"));
					diemHoa = Float.parseFloat(request.getParameter("DiemHoa"));
				} catch (NumberFormatException e) {
					e.printStackTrace();
					out.println("[Sửa] Xảy ra lỗi khi chuyển đổi số liệu: <i>" + e
							+ "</i><hr>");
					out.print("<a href=\"ThaoTac\">Quay lại</a>");
					return;
				}
				String xepLoai = ""; // xếp sau, dựa vào điểm nhập vào
				hocSinh = new HocSinh(hoTen, xepLoai, thuTu, to, diemToan, diemLy,
						diemHoa);
				if (hocSinhBO.suaHocSinh(hocSinh)) {
					// response.sendRedirect("ThaoTac");
					out.print("Sửa thành công!<hr> <a href=\"ThaoTac\"> Quay lai </a>");
				} else {
					out.print("Sửa thất bại!<hr> <a href=\"ThaoTac\"> Quay lai </a>");
				}
			}
			} // End Switch

		} // End If

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
