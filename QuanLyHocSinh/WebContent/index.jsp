<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>QUẢN LÍ HỌC SINH LỚP 10A, BAN TỰ NHIÊN</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<h1 class="text text-warning text-center">
			<a href="ThaoTac" target="_main"><span
				class="glyphicon glyphicon-home" aria-hidden="true"></span></a> QUẢN LÍ
			HỌC SINH LỚP 10A, BAN TỰ NHIÊN
		</h1>
		<div class="divider">
			<hr>
		</div>
	</div>
	<!-- HEAD : Phần tiêu đề bên trên-->
	<div class="container-fluid" style="width: 97%;">
		<div class="row">
			<div class="col-md-1">STT</div>
			<div class="col-md-3">Họ tên</div>
			<div class="col-md-1">Tổ</div>
			<div class="col-md-1">Điểm Toán</div>
			<div class="col-md-1">Điểm Lý</div>
			<div class="col-md-1">Điểm Hóa</div>
			<div class="col-md-2">Xếp loại</div>
			<div class="col-md-1">Xóa?</div>
			<div class="col-md-1">Thao tác</div>
		</div>
	</div>
	<!-- /HEAD -->

	<div class="container-fluid">

		<iframe src="ThaoTac" name="_main"
			style="border: none; width: 100%; height: 300px;"> </iframe>

	</div>
	<div class="container">
		<div class="divider">
			<hr>
		</div>
		<div class="text text-info">
			Quy tắc xếp loại: có các mốc quan trọng là &lt;5.0 - Yếu, 5.0 - TB,
			6.5 - Khá, 8.0 - Giỏi, 9.0 - Xuất sắc. <br> Điểm Trung Bình và
			điểm thành phần phải vượt mốc, chỉ cần 1 môn ở mốc nào thì kết quả
			xếp loại ở mốc đó.
		</div>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>