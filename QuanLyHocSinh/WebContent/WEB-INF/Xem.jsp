<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.HocSinh"%>
<%@ page import="java.util.ArrayList"%>
<%
	ArrayList<HocSinh> cacHocSinh = (ArrayList<HocSinh>) request
			.getAttribute("cacHocSinh");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>QUẢN LÍ HỌC SINH LỚP 10A, BAN TỰ NHIÊN</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container-fluid">
		<!-- ADDNEW -->
		<div class="row">
			<form action="ThaoTac" method="get" class="form form-horizontal"
				role="form">
				<div class="col-md-1">
					<input class="form-control" type="text" name="ThuTu" value="0" readonly>
				</div>
				<div class="col-md-3">
					<input class="form-control" type="text" name="HoTen" value="">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="To" value="0">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="DiemToan" value="0">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="DiemLy" value="0">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="DiemHoa" value="0">
				</div>
				<div class="col-md-2">
					<input class="form-control" type="text" name="XepLoai" value="tự động xếp" readonly>
				</div>
				<div class="col-md-2">
					<input type="hidden" name="GuiBieuMau" value="Co"> <input
						type="hidden" name="LoaiBieuMau" value="Them"> <input
						class="form-control btn btn-info btn-block" type="submit"
						value="Thêm mới">
				</div>
			</form>
		</div>
		<div class="divider">
			<hr>
		</div>
		<!-- /ADDNEW -->

		<!-- RECORD: cứ 1 bản ghi nằm trên 1 dòng, dùng vòng for để đưa dữ liệu ra -->
		<%
			for (int i = 0; i < cacHocSinh.size(); i++) {
				HocSinh hs = cacHocSinh.get(i);
		%>
		<div class="row">
			<form action="ThaoTac" method="get" class="form form-horizontal"
				role="form">
				<div class="col-md-1">
					<input class="form-control" type="text" name="ThuTu"
						value="<%=hs.getThuTu()%>" readonly>
				</div>
				<div class="col-md-3">
					<input class="form-control" type="text" name="HoTen"
						value="<%=hs.getHoTen()%>">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="To"
						value="<%=hs.getTo()%>">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="DiemToan"
						value="<%=hs.getDiemToan()%>">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="DiemLy"
						value="<%=hs.getDiemLy()%>">
				</div>
				<div class="col-md-1">
					<input class="form-control" type="text" name="DiemHoa"
						value="<%=hs.getDiemHoa()%>">
				</div>
				<div class="col-md-2">
					<input class="form-control" type="text" name="XepLoai" value="<%=hs.getXepLoai()%>" readonly>
				</div>
				<div class="col-md-1">
				<input type="hidden" name="GuiBieuMau" value="Co"> 
					<input class="form-control" type="checkbox" name="LoaiBieuMau"
						value="Xoa">
				</div>
				<div class="col-md-1">
					<input class="form-control btn btn-success" type="submit"
						value="Lưu">
				</div>
			</form>
		</div>
		<%
			}
		%>
		<!-- /RECORD -->
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>