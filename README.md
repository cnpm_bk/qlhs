Chú ý về cài đường dẫn CSDL:

- Hiện tại repo vẫn dùng phương pháp truy xuất trực tiếp từ file, do đó phải cài đường dẫn đúng tới tệp CSDL, nếu ko sẽ xuất hiện lỗi 500.

- Mở **/src/Model/DAO/CoSoDuLieu.java**

- Tìm đến dòng 29: **String address = ....**

- Sửa đường dẫn tới tệp CSDL nằm trong thư mục **/WebContent/WEB-INF/**